//
// Created by Никита Кобик on 28.11.2023.
//

#include <stdlib.h>
#include <assert.h>
#include "mem.h"
#include "mem_internals.h"
#include "test.h"


static void check_mem_not_null ( void* ptr ) {
    assert( ptr != NULL );
}

static void check_mem_emptiness ( void* ptr, size_t size ) {
    for ( size_t i = 0; i < size; ++i ) {
        assert( *( (char*) ptr + i ) == 0 );
    }
}


void basic_allocation ( void ) {
    void* mem = _malloc( SMALL_BLOCK_SIZE );

    check_mem_not_null( mem );
    check_mem_emptiness( mem, SMALL_BLOCK_SIZE );

    _free( mem );
}

void basic_reallocation ( void ) {
    void* mem = _malloc( SMALL_BLOCK_SIZE );

    check_mem_not_null( mem );
    check_mem_emptiness( mem, SMALL_BLOCK_SIZE );

    _free( mem );

    mem = _malloc( MID_BLOCK_SIZE );
    check_mem_not_null( mem );
    check_mem_emptiness( mem, MID_BLOCK_SIZE );

    _free( mem );
}

void several_allocations ( void ) {
    void* mem1 = _malloc( SMALL_BLOCK_SIZE );
    void* mem2 = _malloc( SMALL_BLOCK_SIZE );
    void* mem3 = _malloc( SMALL_BLOCK_SIZE );

    check_mem_not_null( mem1 );
    check_mem_not_null( mem2 );
    check_mem_not_null( mem3 );

    check_mem_emptiness( mem1, SMALL_BLOCK_SIZE );
    check_mem_emptiness( mem2, SMALL_BLOCK_SIZE );
    check_mem_emptiness( mem3, SMALL_BLOCK_SIZE );

    _free( mem1 );
    _free( mem2 );
    _free( mem3 );
}

void several_different_allocations ( void ) {
    void* mem1 = _malloc( SMALL_BLOCK_SIZE );
    void* mem2 = _malloc( MID_BLOCK_SIZE );
    void* mem3 = _malloc( BIG_BLOCK_SIZE );

    check_mem_not_null( mem1 );
    check_mem_not_null( mem2 );
    check_mem_not_null( mem3 );

    check_mem_emptiness( mem1, SMALL_BLOCK_SIZE );
    check_mem_emptiness( mem2, MID_BLOCK_SIZE );
    check_mem_emptiness( mem3, BIG_BLOCK_SIZE );

    _free( mem1 );
    _free( mem2 );
    _free( mem3 );
}

void sequential_allocations ( void ) {
    void* mem = _malloc( SMALL_BLOCK_SIZE );

    check_mem_not_null( mem );
    check_mem_emptiness( mem, SMALL_BLOCK_SIZE );

    mem = _malloc( BIG_BLOCK_SIZE );

    check_mem_not_null( mem );
    check_mem_emptiness( mem, BIG_BLOCK_SIZE );

    _free( mem );
}

void large_allocation ( void ) {
    void* mem = _malloc( LARGE_BLOCK_SIZE );

    check_mem_not_null( mem );
    check_mem_emptiness( mem, LARGE_BLOCK_SIZE );

    _free( mem );
}

void one_more_test ( void ) {
    void* mem1 = _malloc( SMALL_BLOCK_SIZE );
    void* mem2 = _malloc( MID_BLOCK_SIZE );

    check_mem_not_null( mem1 );

    _free( mem1 );

    void* mem3 = _malloc( BIG_BLOCK_SIZE );

    check_mem_not_null( mem2 );
    check_mem_not_null( mem3 );

    check_mem_emptiness( mem2, MID_BLOCK_SIZE );
    check_mem_emptiness( mem3, BIG_BLOCK_SIZE );

    _free( mem2 );
    _free( mem3 );
}

void start_testing ( void ) {
    heap_init(REGION_MIN_SIZE);

    basic_allocation();
    basic_reallocation();
    several_allocations();
    several_different_allocations();
    sequential_allocations();
    large_allocation();
    one_more_test();
}
